# Copyright 2022-Coopdevs Treball SCCL (<https://coopdevs.org>)
# - César López Ramírez - <cesar.lopez@coopdevs.org>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Odoo Femprocomuns Custom Module",
    "version": "12.0.1.1.2",
    "depends": ["easy_my_coop"],
    "author": "Coopdevs Treball SCCL",
    "category": "Discuss",
    "website": "https://coopdevs.org",
    "license": "AGPL-3",
    "summary": """
        Odoo Femprocomuns Custom Module
    """,
    "data": [
        "views/subscription_template.xml",
        "views/subscription_request_view.xml",
    ],
    "installable": True,
}
